require_relative '../modules/console_module'
require_relative '../modules/scraper_module'
require_relative 'team'

class Manager

  def initialize
    @bot = ScraperModule.create_robot
    @bot.goto ScraperModule.url[:index]
  end

  # find team, return results of them and the rival name
  def load_team
    team = Team.new ConsoleModule.team_name, @bot
    response = team.search_team
    if team.search_team  == false
      ConsoleModule.error_team
    else
      team.select_team ConsoleModule.menu(response)
      # @bot.screenshot.save('ss.png')
      {results_user_team: team.results, rival_name: team.next_match}
    end
  end

  # find rival_team results
  def rival_team rival_name
    team = Team.new rival_name, @bot
    if team.search_team == false
      ConsoleModule.internet_error
    else
      team.select_team '1'
      team.results
    end
  end 

  # return the final probability with two results and rival name
  def probability user_team, rival_team, rival_name
    total = user_team + rival_team
    probability = ((user_team/total)*100).round(1)
    ConsoleModule.show_results(rival_name, probability.to_s)
  end

end
