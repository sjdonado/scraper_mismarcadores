require_relative '../modules/scraper_module'

class Team

    # attr_reader :bot

    #builder
    def initialize name_team, bot
        @bot = bot
        @name_team = name_team
    end
    
    # return a array with the options of name team
    def search_team
        @bot.element(css: '#lsid-box > span.control-panel-icon.control-panel-icon-last > a').click
        sleep 1
        @bot.text_fields[0].set @name_team
        @bot.element(css: '#search-form-submit-button').click
        sleep 1
        if @bot.element(css: '#search-results > div:nth-child(1) > table > tbody > tr:nth-child(1) > td').present?
            response_html = ScraperModule.parse_html @bot
            teams = Array.new
            response_html.css('#search-results > div.search-result-wrapper > table:nth-child(2) > tbody > tr').each do |name| 
                teams << name.text
            end
            return teams
        else
            return false
        end
    end

    # with the array, return the team of user in the page
    def select_team option
        sleep 1
        i = 2
        i += 1 unless @bot.element(css: '#search-results > div:nth-child(1) > table:nth-child(2) > thead > tr > th').text == 'Equipos'
        name_select = @bot.element(css: '#search-results > div.search-result-wrapper > table:nth-child(' + i.to_s + ') > tbody > tr:nth-child(' + option + ') > td > a').text
        @name_team = name_select.split(' (')[0]
        @bot.element(css: '#search-results > div.search-result-wrapper > table:nth-child(' + i.to_s + ') > tbody > tr:nth-child(' + option + ') > td > a').click        
    end

    # find the results with the table of the last 10 scores of team
    def results
        page_html = ScraperModule.parse_html @bot
        percent = 0
        page_html.css('#fs-summary-results > table > tbody > tr').each do |row|
            row.css('td > span').each do |col|
                percent += 1 if !col.attr('title').nil? && col.attr('title') == "Ganado" 
            end
        end
        return (percent.to_f/10)*100
    end

    # find the next match with the second frame
    def next_match
        @bot.element(css: '#fs-summary-fixtures > table > tbody > tr').click
        teams = @bot.element(css: '#fs-summary-fixtures > table > tbody > tr').text.strip.split("\n")
        if teams[1].include? @name_team
            return teams[2]
        else
            return teams[1]
        end       
    end

end