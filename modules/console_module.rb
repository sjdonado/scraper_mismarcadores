require 'console_view_helper'

module ConsoleModule

  #get team name
  def self.team_name
    puts 'Enter the name'
    ConsoleViewHelper.input
  end

  #show menu
  def self.menu array
    puts ConsoleViewHelper.menu(array, li_gap: 1)
    puts 'Select a option'
    return ConsoleViewHelper.input
    puts 'Loading...'
  end

  #team not found
  def self.error_team
    puts 'Team not found'
  end

  #internet error
  def self.internet_error
    puts 'Error with your internet conection'
  end

  #print results
  def self.show_results rival_name, probability
    puts 'Probability to win vs ' + rival_name + ' = ' + probability + '%'
  end

  # def self.clean_screen
  #   system('clear')
  # end

end
