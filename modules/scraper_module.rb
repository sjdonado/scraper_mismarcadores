require 'watir'
require 'nokogiri'
require_relative 'console_module'

module ScraperModule

  # attr_reader :url

  @@url = {
    index: 'http://www.mismarcadores.com/'
  }

  def self.create_robot
    # phantomjs firefox
    browser = ::Watir::Browser.new :phantomjs
    browser.window.maximize
    browser
  end

  def self.clear browser
    browser.cookies.clear
    browser
  end

  # parse html to nokogiri object
  def self.parse_html browser
    sleep 1
    Nokogiri::HTML.parse browser.html
  end

  def self.url
    @@url
  end

end
