require_relative 'classes/manager'

manager = Manager.new
load_team = manager.load_team
results_rival_team = manager.rival_team load_team[:rival_name]
manager.probability load_team[:results_user_team], results_rival_team, load_team[:rival_name]
